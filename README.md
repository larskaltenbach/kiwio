# kiwio

[![](http://img.youtube.com/vi/IfSORJ93twQ/0.jpg)](http://www.youtube.com/watch?v=IfSORJ93twQ "Kiwio Concept")


## Todo:
- [x] Hardware: Redesign case
- [x] Hardware: BOM prototype
- [x] Hardware: Print parts
- [x] Hardware: Assemble parts
- [ ] Hardware: polish and paint

- [x] Software: Settings file 
- [x] Software: Fix crash when time is up
- [x] Software: Make time budget persistent across reboots

- [x] Infrastructure: Create disk image
- [x] Infrastructure: Write documentation


## Firmware
The heart: Controls screen, key, time management and system services.

## Documentation

### Installation

Download Kiwio image:  
..... 

Folge dem Raspberry pi guide (mit dem Kiwio image):  
[Raspberry Pi Documentation](https://www.raspberrypi.com/documentation/computers/getting-started.html)  
diskutil list  
diskutil unmountDisk /dev/diskN  
sudo dd bs=1m if=kiwio_2021-10-18.img of=/dev/rdiskN; sync  
 
Jetzt nur noch die Einstellungen anpassen (s.u.)


### Allgemeines
Die Kiwio-Box startet sich automatisch jeden Tag um 4:00 Uhr selbst, um einen längeren Betrieb zugewährleisten und die Log/Konfigurationsdateien täglich neu zu lesen.
(Kann in /etc/systemd/system/sched-reboot.timer geändert werden)

Wenn man die Raspberry Pi SD Karte liest, bekommt man das Volume *boot* angezeigt.
Hier liegen alle Daten, die der Raspberry als Einstellungen braucht.
Dieses Volume beinhaltet zwei relevante Dateien:

### kiwio.text
Das sind die Einstellungen der Kiwio-Box. 
Die Formatierung der Datei sollte beibehalten werden (keine Leerzeichen, 4 Zeilen). 

*budget-in-minutes* Ist das tägliche Zeitbudget  
*x-offset* verschiebt die Display anzeige in der X-Achse  
*y-offset* verschiebt die Display anzeige in der Y-Achse  
*log_interval_in_seconds* Stellt ein wie oft die Log Datei geschrieben wird 


### kiwio_log  
Die Datei speichert das verbrauchte Zeitbudget über einen Reboot hinaus.
Eigentlich muss diese Datei nicht editiert werden, kann aber zu Testzwecken geändert werden. 

Das Format ist immer: Datum+Zeitzonen-Offset + verbrauchte Millisekunden 

Das Kiwio Programm benutzt die Logdatei in etwa so:  
Wenn das Datum dem heutigen entspricht und schon Budget verbraucht wurde, wird es vom Tagesbudget der Einstellunngsdatei abgezogen. Wenn es nicht dem heutigen DAtum entspricht wird das volle Budget genommen und die Logdatei überschrieben. 
 
Es kann vorkommen, dass eine leere Logdatei geschrieben wird, wenn genau im Schreibprozess die Stromzufuhr unterbrochen wird. In diesem Fall kann die Logdatei einfach händisch gelöscht werden, oder ein fiktives verbrauchtes Budget eingetragen werden.
Daher sollte *log_interval_in_seconds* in *boot/kiwio.txt* möglichst hoch gesetzt werden. Das hat allerdings den Nachteil, dass bei einem Stromverlust eventuell entsprechend mehr Zeitbudget zur Verfügung steht. 


## Web Interface
Next iteration

## Product Design
Known limitations:
- Top is detachable -> next iteration should include stronger snap-mechanic

