use display_interface_spi::SPIInterfaceNoCS;
use embedded_graphics::{
    pixelcolor::{Rgb565},
    prelude::*,
    mono_font::{ascii::FONT_10X20, MonoTextStyleBuilder},
    text::{Alignment, Text},
    primitives::{Circle, Arc, PrimitiveStyleBuilder, StrokeAlignment},
};
use st7789::{Orientation, ST7789};
use rppal::{ 
    spi::{Bus, Mode, SlaveSelect, Spi},
    gpio::Gpio,
    hal::Delay,
};
use std::time::{ Duration, Instant};
use std::error::Error;
use stopwatch::{Stopwatch};

use std::process::Command;
use std::fs;

use std::convert::TryInto;


fn main() -> Result<(), Box<dyn Error>> {
    let settings_file = fs::read_to_string("/boot/kiwio.txt")
        .expect("Something went wrong reading the file");

    let mut settings = Vec::<i32>::new();
    
    for line in settings_file.lines() {
            let strings: Vec<&str> = line.trim().split("=").collect();
            settings.push( strings[1].parse().unwrap() );
    }

    let mut sw = Stopwatch::new();

    let budget_from_settings = Duration::new(settings[0] as u64 * 60, 0); 
    let initial_used_budget = Duration::from_millis( check_used_budget() );

     let budget_from_files = 
         match budget_from_settings.checked_sub( initial_used_budget ) {
            None => Duration::from_millis(0),
            Some(x) => x,
        };

    let time_budget = budget_from_files;

    println!("Hello, Kiwio!\n-----\nTime budget: {}\nAlready used today: {}\nLog interval: {}s\n-----", format_time(budget_from_settings), format_time(initial_used_budget), settings[3] );

    let mut state: i32 = -1; // 0: time up, 1: running, 2: paused, -1: start up

    let mut last_debounce_time: Instant = Instant::now(); 
    let debounce_delay = Duration::from_millis(50); 
    let mut button_state: u8 = 0;
    let mut last_button_state: u8 = 0;


    let spi = Spi::new(Bus::Spi0, SlaveSelect::Ss0, 32_000_000, Mode::Mode3)?;
    let dc = Gpio::new()?.get(25)?.into_output();
    let rst = Gpio::new()?.get(27)?.into_output();

    let key = Gpio::new()?.get(21)?.into_input();

    let mut delay = Delay::new();

    let di = SPIInterfaceNoCS::new(spi, dc);
    let mut display = ST7789::new(di, rst, 240, 240);

    let circle_stroke = PrimitiveStyleBuilder::new()
        .stroke_color(Rgb565::new(200,200,200))
        .stroke_width(10)
        .stroke_alignment(StrokeAlignment::Inside)
        .build();

    let circle_clear = PrimitiveStyleBuilder::new()
        .fill_color(Rgb565::BLACK)
        .build();

    let arc_stroke = PrimitiveStyleBuilder::new()
        .stroke_color(Rgb565::BLACK)
        .stroke_width(10)
        .stroke_alignment(StrokeAlignment::Inside)
        .build();

    let character_style = MonoTextStyleBuilder::new()
        .font(&FONT_10X20)
        .text_color(Rgb565::WHITE)
        .background_color(Rgb565::BLACK)
        .build();


    let center = display.bounding_box().center() + Point::new(settings[1], settings[2]); //Point::new(-4, 80);


    display.init(&mut delay).unwrap();
    display.set_orientation(Orientation::PortraitSwapped).unwrap();
    display.clear(Rgb565::BLACK).unwrap();
    Circle::with_center(center, 180 - 10)
        .into_styled(circle_stroke)
        .draw(&mut display).unwrap();

    let mut interval_millis = 0;

    loop {

        if sw.elapsed() + Duration::from_millis(100) >= time_budget {  
            stop(&mut state); 
        }


// ----- READ KEY PRESS

        let reading: u8 = key.read() as u8;

        if reading != last_button_state {
            last_debounce_time = Instant::now();
        }

        if (Instant::now() - last_debounce_time) > debounce_delay {
            if reading != button_state {
                button_state = reading;

                if button_state == 1 {

                   Circle::with_center(center, 180 - 20)
                        .into_styled(circle_clear)
                        .draw(&mut display).unwrap();
                    
                   if state == 2 || state == -1 {
                       //-> START 
                       start(&mut state, &mut sw);
                    } else if state == 1 {
                       //-> PAUSE 
                        pause(&mut state, &mut sw);
                    }
                }
            }
        }

        last_button_state = reading;

// ----- LOG THE USED TIME

        let time_used = sw.elapsed();


        let current_millis = sw.elapsed().as_millis();
        if current_millis - interval_millis >= (settings[3] * 1000).try_into().unwrap() {
            println!("log"); 
            log_used_budget(&time_used, &initial_used_budget); 
            interval_millis = current_millis;
        }
        


// ----- DISPLAY PROGRESS

        if state != 0 {
            let progress: f32;
            progress = (time_used.as_millis() as f32 / time_budget.as_millis() as f32).clamp(0.0,100.0); 

            let sweep = progress as f32 * 360.0;

            if progress != 100.0 {
                Arc::with_center(center, 180 - 10, 90.0.deg(), sweep.deg())
                    .into_styled(arc_stroke)
                    .draw(&mut display).unwrap();
            }
        } else {
             Arc::with_center(center, 180 - 10, 90.0.deg(), 360.0.deg() )
                .into_styled(arc_stroke)
                .draw(&mut display).unwrap();
        }



        


// ----- DISPLAY STATE

        let text = match state {
            1 => format_time(time_budget - sw.elapsed()),
            2 => "Pausiert".to_string(),
            -1 => "Starte mich".to_string()+ "\n" + &format_time(time_budget - sw.elapsed()) ,
            0 => "Zeitbudget\nverbraucht".to_string(),
            _ => "Oops".to_string(),
        };

        Text::with_alignment(
            &text,
            center,
            character_style,
            Alignment::Center,
            ).draw(&mut display).unwrap();
    }
}


// ----- HELPER FOR TIME FORMAT

fn format_time(duration: Duration) -> String {

    let seconds = duration.as_secs() % 60;
    let minutes = duration.as_secs() / 60 % 60;
    let hours = duration.as_secs() / 60 / 60;

    format!("-{:0>2}:{:0>2}:{:0>2}", hours, minutes, seconds) 
}


// ----- START TIMER

fn start(state: &mut i32, sw: &mut Stopwatch) {
    println!("start");
    *state = 1;
    sw.start();

    Command::new("sudo")
        .arg("/bin/systemctl")
        .arg("start")
        .arg("hostapd")
        .spawn()
        .expect("can't initialize hotspot");
}

// ----- PAUSE TIMER

fn pause(state: &mut i32, sw: &mut Stopwatch) {
    println!("pause");
    *state = 2;
    sw.stop();

    Command::new("sudo")
        .arg("/bin/systemctl")
        .arg("stop")
        .arg("hostapd")
        .spawn()
        .expect("can't stop hotspot");
}



// ----- STOP TIMER

fn stop(state: &mut i32) {
    
    if *state != 0 {
        println!("stop");

        Command::new("sudo")
            .arg("/bin/systemctl")
            .arg("stop")
            .arg("hostapd")
            .spawn()
            .expect("can't stop hotspot");

        *state = 0;
    }
}

// ----- LOG USED BUDGET

fn log_used_budget(used_time: &Duration, initial_used_time: &Duration) {

   let total_used_time = *used_time + *initial_used_time; 

   let mut out = chrono::offset::Local::now().date().to_string();
    out += " ";
    out += &total_used_time.as_millis().to_string();
    out += "\n";

    fs::write("/boot/kiwio_log", out).expect("Unable to write file");
}


// ----- READ USED BUDGET

fn check_used_budget() -> u64 {
    let data = fs::read_to_string("/boot/kiwio_log").unwrap_or("x 0".to_string());
    let strings: Vec<&str> = data.split_whitespace().collect();
          
    let mut used_budget: u64 = 0; 

    if strings[0] == chrono::offset::Local::now().date().to_string() {
        used_budget = strings[1].parse::<u64>().unwrap();
    }

    used_budget
}

